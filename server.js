var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var routes = require('./api/routes/gist');
routes(app);

routes = require('./api/routes/scoreboard');
routes(app);

app.listen(port);

app.use(function(req, res) {
    res.status(404).send({
        status: false,
        data: req.originalUrl + ' was not found.'
    })
});

console.log('DEVGRID Tech Test started on port : ' + port);