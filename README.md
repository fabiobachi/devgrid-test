# README #

This repository contains a development test for DEVGRID, Developed by F�bio Bachi.
Inside, you'll find a API developed in NodeJS, containing two modules:
* Gists: create Gists and list comments from specific Gist.
* Scoreboard: input and output results for a simple problems/enigmas solving.

### REQUIREMENTS ###

* NodeJS 8.11

### CONFIG (must do) ###

* Github account can be changed in the `Gists/Add` request headers in Postman.

### RUN ###

* First of all, run `npm i` inside the project.
* Then simply run `npm start` with a command prompt inside the root folder.
* Methods can be tested with the postman file found in the root folder.

### RUN TESTS ###

* Run `npm test` with a command prompt inside the root folder.
* Testing scripts can be found inside api/tests/tests.js
* For testing `Gists/Add`, the user and password must be changed inside tests.js.