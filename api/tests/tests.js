'use strict';

const gistModel = require('../models/gist');
const scoreboardModel = require('../models/scoreboard');

const test = require('blue-tape');

/**
 * Unit tests for each one of the four methods contained in this application.
 */

test("Gist/Add", function(t) {
    return gistModel.addGist({
        description: 'From unit test',
        files: [{
            name: 'index.php',
            content: '<?php'
        }]
    }, {
        githubemail: 'fabio.bachi@gmail.com',
        githubpassword: 'password'
    });
});

test("Gist/List comments", function(t) {
    return gistModel.getGist('862a1df2213dacc94470739a891e8bad');
});

test("Scoreboard/Input", function(t) {
    return scoreboardModel.addInput({
        contestant: 1,
        problem: 1,
        time: 25,
        action: 'R'
    });
});

test("Scoreboard/Output", function(t) {
    return scoreboardModel.result();
});