'use strict';

const gistModel = require('../models/gist');

exports.add = (req, res) => {

    gistModel.addGist(req.body, req.headers).then((response) => {
        res.json({
            status: response ? true : false,
            data: response ? response : 'An error ocurred.'
        });
    }).catch(error => {
        res.json({
            status: false,
            data: error
        });
    });
};

exports.getComments = (req, res) => {
    const gistId = req.params.gistId ? req.params.gistId : null;

    gistModel.getGist(gistId).then((response) => {
        res.json({
            status: true,
            data: response
        });
    }).catch(error => {
        res.json({
            status: false,
            data: error
        });
    });

};