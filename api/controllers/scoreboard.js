'use strict';

const scoreboardModel = require('../models/scoreboard');

exports.add = (req, res) => {
    const post = req.body;

    scoreboardModel.addInput(post).then((response) => {
        res.json({
            status: response ? true : false,
            data: response ? response : 'An error ocurred.'
        });
    }).catch(error => {
        res.json({
            status: false,
            data: error
        });
    });
};

exports.result = (req, res) => {
    scoreboardModel.result().then((response) => {
        res.json({
            status: response ? true : false,
            data: response ? response : 'An error ocurred.'
        });
    }).catch(error => {
        res.json({
            status: false,
            data: error
        });
    });
};
