'use strict';

exports.addInput = (post) => {
    const path = './scoreboard/scoreboard.json',
          fs = require('fs');
    return new Promise((resolve, reject) => {

        const actions = ['C', 'I', 'R', 'U', 'E'];

        if (!post.contestant || !post.problem || !post.time || !post.action){
            reject('A input requires a contestant, a problem, the answer time and action.');
        } else if (!actions.includes(post.action)){
            reject('The action string must be one of the following letters: C, I, R, U, E.');
        }

        // Reads json file from server
        fs.readFile(path, 'utf8', function (error, arraySort) {
            var contestant = {};

            if (error)
                arraySort = [];
            else{
                arraySort = JSON.parse(arraySort ? arraySort : '[]');
            }

            var data = {};
            // Puts each contestant inside the `data` object
            // so the following scripts can access a specific contestant.
            arraySort.forEach((el, i) => {
                data[el.contestant] = el;
            })

            if (data[post.contestant]){
                // Existing contestant
                contestant = data[post.contestant];
            } else {
                // If it's a new contestant
                contestant = {
                    contestant: post.contestant,
                    problemsSolved: 0,
                    penalty: 0
                };
            }

            // Calculate the current contestant's score and penalty.
            switch (post.action){
                case 'C': // Correct
                    contestant.problemsSolved++;
                    contestant.penalty += parseFloat(post.time);
                    break;
                case 'I': // Incorrect
                    contestant.penalty += 20;
                    break;
            }

            data[post.contestant] = contestant;

            var arraySort = Object.values(data);
            data = {};

            // Sorting script
            arraySort.sort(function(a, b) {
                // Sorts by problems solved (DESC)
                var test = ((a["problemsSolved"] - b["problemsSolved"]) * -1);
                if (Math.abs(test) != 0)
                    return test;
                else {
                    // Sort by penalty (ASC)
                    test = a["penalty"] - b["penalty"];
                    if (test != 0)
                        return test;
                    else {
                        // Sorts by contestant (ASC)
                        return a["contestant"] - b["contestant"];
                    }
                }
            });

            // Writes result in file
            fs.writeFile(path, JSON.stringify(arraySort), 'utf8', (err) => {
                resolve('Scoreboard updated.');
            });
        });
    });
}

exports.result = (post) => {
    const path = './scoreboard/scoreboard.json',
          fs = require('fs');
    return new Promise(resolve => {
        fs.readFile(path, 'utf8', function (error, data) {
            var contestant = {};

            if (error)
                data = [];
            else{
                data = JSON.parse(data ? data : '[]');
            }

            resolve(data);
        });
    });
}