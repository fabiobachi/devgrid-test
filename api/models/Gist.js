'use strict';

const GitHub = require('github-api');

/**
 * Adds a gist to the user's account.
 * @author Fábio Bachi [bachi@ezoom.com.br]
 * @date        2018-04-22
 * @param       {object}   post
 * @param       {object}   headers
 * @return      {promise}
 */
exports.addGist = (post, headers) => {

    return new Promise((resolve, reject) => {

        if (!post.description){
            reject('A gist needs a title.');
        }

        if (!post.files || !post.files.length){
            reject('At least one file must be sent to a gist.');
        }

        const github = new GitHub({
            username: headers.githubemail,
            password: headers.githubpassword
        });

        // Creates a blank gist object
        let gist = github.getGist();

        let gistData = {
            public: true,
            description: post.description,
            files: {}
        };

        // Validation for each files sent.
        post.files.forEach((el, i) => {
            if (el.name && el.content)
                gistData.files[el.name] = { content: el.content };
        });

        gist.create(gistData).then(function({data}) {
            let createdGist = data;
            return gist.read();
        }).then(function({data}){
            resolve(data);
        }).catch((response) => {
            resolve(false);
        });
    });
}

exports.getGist = (gistId) => {

    const github = new GitHub();
    var gist = github.getGist(gistId, github),
        comments = [];

    return new Promise((resolve, reject) => {
        if (!gistId){
            reject('A gist ID is required.');
        }

        gist.listComments(data => {
            return data;
        }).then((response) => {
            if (response.data)
                comments = response.data;
            resolve(comments);
        }).catch((response) => {
            resolve([]);
        });
    });
}
