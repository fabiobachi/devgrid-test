'use strict';

module.exports = function(app) {
    var gist = require('../controllers/scoreboard');

    app.route('/scoreboard/add')
        .post(gist.add);

    app.route('/scoreboard/result')
        .get(gist.result);
};
