'use strict';

module.exports = function(app) {
    var gist = require('../controllers/gist');

    app.route('/gist/add')
        .post(gist.add);

    app.route('/gist/:gistId/comments')
        .get(gist.getComments);
};
